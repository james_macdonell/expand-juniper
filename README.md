
    Usage: expand-juniper.pl [-D] [-e] [-E] [-F <format>] [-h] [-z <targetzone>] -f <filename> -t <targetobj> [-w] [-s] [-d]
        -D      Turn on debugging to STDERR.
        -e      Expand all group and address object to CIDR notation.
        -E      Outputs both the compact and expanded forms of the reports.
        -F      Specify the output format. Currently supports 'csv', 'html', and 'table'.
        -h      Help; Show this message.
        -z      The name of the security zone where the target IP (-t) is located,
                otherwise the script assumes that the IP can be reached through any zone.
                This option is recommended, otherwise all 'Any' entries will match.
        -f      The relative path to the filename for processing.
        -t      The target IP address or CIDR subnet to be used for policy matching.
                Examples:       192.168.10.4 (single IP, assumed /32 mask)
                                192.168.10.0/27 (entire /27 range must be matched)
                                192.168.10.0/255.255.255.224 (same as using /27)
                                fe80::/64 (IPv6)
                                any or 0.0.0.0/0 (will match all policies)
        -w      Matches <targetobj> or anything within <targetobj>.
                For example -t 192.168.10.0/24 -w will match a rule using specifically 192.168.10.99
        -s      Limit matches to policy source addresses (default is both source and destination)
        -d      Limit matches to policy destination addresses

                Initially written by James Schneider (jrschneider@csupomona.edu) , January 2010
                Patch contribution by James M.
